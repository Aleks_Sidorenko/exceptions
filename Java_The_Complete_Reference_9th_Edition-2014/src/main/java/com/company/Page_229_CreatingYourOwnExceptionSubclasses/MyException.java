package com.company.Page_229_CreatingYourOwnExceptionSubclasses;

/**
 * Created on 06.08.2020 17:29.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */
public class MyException extends Exception {

    private int detail;

    MyException(int a) {
        detail = a;
    }

    public String toString() {
        return "MyException[" + detail + "]";
    }
}
