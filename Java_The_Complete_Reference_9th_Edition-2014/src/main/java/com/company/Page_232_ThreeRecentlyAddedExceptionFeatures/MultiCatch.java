package com.company.Page_232_ThreeRecentlyAddedExceptionFeatures;

/**
 * Created on 06.08.2020 17:46.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */
public class MultiCatch {

    public static void main(String[] args) {
        int a = 10, b = 0;
        int[] vals = {1, 2, 3};
        try {
            vals[3] = 4;
            int result = a / b; // generate an ArithmeticException
            // vals[10] = 19; // generate an ArrayIndexOutOfBoundsException
            // This catch clause catches both exceptions.
        } catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
            System.out.println("Exception caught: " + e);
        }
        System.out.println("After multi-catch.");
    }
}
